class BigThing():
    def __init__(self, value):
        self._value = value
    

    def size(self):
        if type(self._value) == int:
            return(self._value)
        else:
            return(len(self._value))

class BigCat(BigThing):
    def __init__(self, name, wight):
        self._name = name
        self._wight = wight

    def size(self):
        if self._wight > 20:
            return "Very Fat"
        elif self._wight > 15:
            return "Fat"
        else:
            return "OK"
    



def main():
    
    cutie = BigCat("mitzy", 22)
    print(cutie.size())

main()