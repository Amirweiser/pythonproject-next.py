import functools
def add(a, b):
    return int(a) + int(b)

def sum_of_digits(number):
    return functools.reduce(add, str(number))


print(sum_of_digits(104))