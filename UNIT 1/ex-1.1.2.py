
def double_letter(my_str):
    return ''.join(map(lambda x: x*2, my_str))

print(double_letter("python"))