def intersection(list_1, list_2):
    return list(set(filter(lambda x: list_1.count(x) > 0 and list_2.count(x) > 0, list_1)))


print(intersection([1, 2, 3, 4], [8, 3, 9]))
print(intersection([5, 5, 6, 6, 7, 7], [1, 5, 9, 5, 6]))