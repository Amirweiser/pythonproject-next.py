class IDIterator:
    def __init__(self, id):
        self._id = id

    def __iter__(self):
        return self

    def __next__(self):
        while self._id <= 999999999:
            if check_id_valid(self._id):
                valid_id = self._id
                self._id += 1
                return valid_id
            else:
                self._id += 1
        raise StopIteration


def check_id_valid(id_number):
    digits = [int(d) for d in str(id_number)]
    for i in range(9):
        digits[i] *= 1 if i % 2 == 0 else 2
        if digits[i] > 9:
            digits[i] = sum([int(d) for d in str(digits[i])])
    return sum(digits) % 10 == 0


def id_generator(id):
    while id <= 999999999:
        if check_id_valid(id):
            yield id
        id += 1



def main():
    id_num = input("enter ID: ")
    gen_or_Iter = input("Generator or Iterator? (gen/it)?: ")
    if gen_or_Iter == "gen":
        id_gen = id_generator(int(id_num))
        for i in range(10):
            next_id = next(id_gen)
            print(next_id)
    elif gen_or_Iter == "it":
        id_iter = IDIterator(int(id_num))
        for i in range(10):
            next_id = next(id_iter)
            print(next_id)
    else:
        raise TypeError("not a valid response")

if __name__ == "__main__":
    main()