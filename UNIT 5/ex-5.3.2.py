class MusicNotes:
    def __init__(self):
        self.octave = 0
        self.note_index = 0

    def __iter__(self):
        return self

    def __next__(self):
        notes = [("La", 55), ("Si", 61.74), ("Do", 65.41), ("Re", 73.42), ("Mi", 82.41), ("Fa", 87.31), ("Sol", 98)]

        if self.note_index >= len(notes):
            self.note_index = 0
            self.octave += 1

        if self.octave > 4:
            raise StopIteration

        note, frequency = notes[self.note_index]
        frequency *= 2 ** self.octave

        self.note_index += 1

        return frequency
    
def main():
    notes_iter = iter(MusicNotes())
    for freq in notes_iter:
        print(freq)

main()