import datetime

def gen_secs():
    """Returns a generator of all possible seconds (0-59)"""
    return (second for second in range(60))

def gen_minutes():
    """Returns a generator of all possible minutes (0-59)"""
    return (minute for minute in range(60))

def gen_hours():
    """Returns a generator of all possible hours (0-23)"""
    return (hour for hour in range(24))

def gen_time():
    """Generator that generates all possible times in a day"""
    for hour in gen_hours():
        for minute in gen_minutes():
            for second in gen_secs():
                yield datetime.time(hour, minute, second)

def gen_years(start_year=2019):
    """Generator that generates a given year and every year after it"""
    year = start_year
    while True:
        yield year
        year += 1

def gen_months():
    """Returns a generator that generates all the number of months in the year (1-12)"""
    return (month for month in range(1, 13))

def gen_days(month, leap_year):
    """Returns a generator that generates the number of days in a month based on if it's a leap year or not"""
    days_in_month = {
        1: 31,
        2: 29 if leap_year else 28,
        3: 31,
        4: 30,
        5: 31,
        6: 30,
        7: 31,
        8: 31,
        9: 30,
        10: 31,
        11: 30,
        12: 31
    }
    return (day for day in range(1, days_in_month[month] + 1))

def gen_date():
    """Generator that generates a full date in this format : dd/mm/yyyy hh:mm:ss"""
    for year in gen_years():
        for month in gen_months():
            for day in gen_days(month, year % 4 == 0):
                for time in gen_time():
                    yield datetime.datetime(year, month, day, time.hour, time.minute, time.second).strftime('%d/%m/%Y %H:%M:%S')

def main():
    """Main function that prints generated dates every 1000000 iterations"""
    date_gen = gen_date()
    i = 1
    while True:
        date = next(date_gen)
        if i % 1000000 == 0:
            print(date)
        i += 1

if __name__ == '__main__':
    main()
